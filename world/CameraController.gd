extends Camera

export(NodePath) onready var pos_target = self.get_node(pos_target) as Spatial;
export(NodePath) onready var look_target = self.get_node(look_target) as Spatial;
export var FOLLOW_SPEED : float = 10;

onready var look_target_interp : Vector3 = look_target.global_transform.origin;


func _physics_process(delta: float) -> void:
	self.transform.origin = lerp(self.transform.origin, pos_target.global_transform.origin, FOLLOW_SPEED * delta);
	
	look_target_interp = lerp(look_target_interp, look_target.global_transform.origin, FOLLOW_SPEED * delta);
	self.look_at(look_target_interp, Vector3.UP);
	
