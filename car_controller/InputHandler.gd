class_name InputHandler
extends Node

var throtle : float = 0;
var steer : float = 0;
var gear : int = 0;
var breaking : bool = false;
var gearing : bool = false;
var power : bool = false;


func _physics_process(_delta: float) -> void:
	throtle = Input.get_action_strength("reverse") - Input.get_action_strength("accelerate");
	steer = Input.get_action_strength("steer_left") - Input.get_action_strength("steer_right");
	breaking = Input.is_action_pressed("break");
	
	gearing = Input.is_action_pressed("gearing");
	gear = 0;
	if gearing:
		if Input.is_action_just_pressed("gear_up"):
			gear = +1;
		elif Input.is_action_just_pressed("gear_down"):
			gear = -1
	
	power = Input.is_action_just_pressed("power");
