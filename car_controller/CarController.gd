extends KinematicBody

export var START : float = 1;
export var MAX_SPEED : float = 10;
export var MAX_ACCELERATION : float = 1;
export var DECELERATION : float = 1;
export var BREAK : float = 10;
export var STEER : float = 1;
export var STEER_RETURN : float = 10;
export var MAX_GEAR : int = 5;
export(Array, Vector2) var GEAR_SPEED_RANGES;
export var DIE_TIMER_WAIT_TIME : float = 3.0;

onready var input_handler : InputHandler = $InputHandler;
onready var visual : Spatial = $Visual;
onready var die_timer : Timer = $DieTimer;
onready var rpm_rect : ColorRect = $RPMCounterLayer/RPMRect;
onready var power_rect : ColorRect = $PowerLayer/PowerRect;
onready var gearbox_ui : GearboxUI = $GearboxLayer;

var power : bool = false;
var speed : float = 0;
var acceleration : float = 0;
var angle : float = 0;
var dir : Vector3 = Vector3.FORWARD;
var velocity : Vector3 = Vector3.ZERO;
var gear : int = 0;


func _ready():
	# warning-ignore:return_value_discarded
	die_timer.connect("timeout", self, "_on_die_timer_timeout");


func _physics_process(delta: float) -> void:
	power = not power if input_handler.power else power;
	
	if abs(input_handler.throtle) > 0 and input_handler.gearing:
		power = false;
	
	# warning-ignore:narrowing_conversion
	gear = clamp(gear + input_handler.gear, 0, MAX_GEAR);
	
	if power:
		acceleration = lerp(abs(acceleration), abs(input_handler.throtle) * MAX_ACCELERATION, START * delta);
		acceleration *= sign(input_handler.throtle)
	else:
		acceleration = lerp(acceleration, 0, MAX_ACCELERATION * delta);
	
	var max_gear_speed : float = GEAR_SPEED_RANGES[gear].y * MAX_SPEED;
	var min_gear_speed : float = GEAR_SPEED_RANGES[gear].x * MAX_SPEED;
	
	if not power or not abs(acceleration) > 0:
		speed = lerp(speed, 0, DECELERATION * delta);
	elif input_handler.breaking:
		speed = lerp(speed, 0, BREAK * delta);
	else:
		speed = lerp(speed, max_gear_speed * sign(acceleration), abs(acceleration) * delta);
	
	if power and gear > 0:
		angle += input_handler.steer * (abs(speed) / max_gear_speed) * STEER * delta;
	dir = Vector3.FORWARD.rotated(Vector3.UP, angle);
	self.look_at(self.transform.origin + dir, Vector3.UP);
	
	velocity = self.move_and_slide(speed * dir, Vector3.UP);
	
	if abs(speed) < min_gear_speed:
		if die_timer.is_stopped():
			die_timer.start(DIE_TIMER_WAIT_TIME);
	else:
		die_timer.stop();
	
	if gear > 0 and power:
		if abs(speed) > min_gear_speed:
			rpm_rect.rect_scale.y = (abs(speed) - min_gear_speed) / (max_gear_speed - min_gear_speed);
		elif gear > 1:
			rpm_rect.rect_scale.y = - (min_gear_speed - abs(speed)) / min_gear_speed
	else:
		rpm_rect.rect_scale.y = lerp(rpm_rect.rect_scale.y, 0, 5 * delta);
	
	if power:
		power_rect.color.a = lerp(power_rect.color.a, 136.0 / 255.0, 5 * delta);
	else:
		power_rect.color.a = lerp(power_rect.color.a, 0, 5 * delta);
	
	gearbox_ui.gear = gear;


func _on_die_timer_timeout() -> void:
	power = false;
