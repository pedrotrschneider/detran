class_name GearboxUI
extends CanvasLayer

onready var gear_rects = [
	$Gear1,
	$Gear2,
	$Gear3,
	$Gear4,
	$Gear5
]

var gear : int = 0;


func _process(delta):
	for gear_num in gear_rects.size():
		if gear_num >= gear:
			gear_rects[gear_num].color.a = lerp(gear_rects[gear_num].color.a, 0, 5 * delta);
	
	for gear_num in gear:
		gear_rects[gear_num].color.a = lerp(gear_rects[gear_num].color.a, 136.0 / 255.0, 5 * delta);
